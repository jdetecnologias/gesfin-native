import { StatusBar } from 'expo-status-bar';
import React,{useState} from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity} from 'react-native';
import { DataTable } from 'react-native-paper';

export default function App() {
  const [contas, setContas] = useState([
                                        {desc: 'Salário', tipo:'provento', venc:"05/08/2021", valor:"2954,21"},
                                        {desc: 'Casa', tipo:'despesa', venc:"10/08/2021", valor:"410,23"},
                                        {desc: 'Energia', tipo:'despesa', venc:"05/08/2021", valor:"154,21"}
                                      ])

  const [desc,setdesc] = useState('')
  const [venc,setvenc] = useState('')
  const [valor,setvalor] = useState('')

  function adicionarConta(tipo){
    const obj = new Object({desc:desc, venc:venc, valor:valor, tipo:tipo})

    const lcontas = [...contas]

    lcontas.push(obj)

    setContas(contas=>lcontas)

    resetarCampos()
  }

  function resetarCampos(){
    setdesc('')
    setvenc('')
    setvalor('')
  }

  return (
    <View style={styles.container}>
      <Text style={styles.topo}>Gestor financeiro</Text>
      <View>
        <View style={{flexDirection:'row', wrap:'wrap'}}>
          <TextInput value={desc} onChangeText={(text)=>setdesc(text)} placeholder='Descrição' style={{padding:10, width:100, border:1}}/>
          <TextInput value={valor} onChangeText={(text)=>setvalor(text)} placeholder='Valor' style={{padding:10, width:100, border:1}}/>
          <TextInput value={venc} onChangeText={(text)=>setvenc(text)} placeholder='Vencimento' style={{padding:10, width:100, border:1}}/>
        </View>
        <View style={{flexDirection:'row', wrap:'wrap'}}>
          <TouchableOpacity onPress={()=>adicionarConta('provento')} title="+ Provento" style={styles.botoesTopo}><Text style={styles.white}>+ Provento</Text></TouchableOpacity>
          <TouchableOpacity onPress={()=>adicionarConta('despesa')} title="+  Despesa" style={styles.botoesTopo}><Text style={styles.white}>+ Despesa</Text></TouchableOpacity>          
        </View>

      </View>

      <View style={styles.gridContas}>
        <DataTable>
          <DataTable.Header>
            <DataTable.Title>Descrição</DataTable.Title>
            <DataTable.Title>Vencimento</DataTable.Title>
            <DataTable.Title>Provento</DataTable.Title>
            <DataTable.Title>Despesa</DataTable.Title>
          </DataTable.Header>
          {
            contas.map(conta=>{

              return(
                <DataTable.Row>
                <DataTable.Cell>{conta.desc}</DataTable.Cell>
                <DataTable.Cell>{conta.venc}</DataTable.Cell>
                <DataTable.Cell >{conta.tipo === 'provento'?conta.valor:''}</DataTable.Cell>
                <DataTable.Cell >{conta.tipo === 'despesa'?conta.valor:''}</DataTable.Cell>
              </DataTable.Row>
              )
            })
          }
        </DataTable>
      </View>


    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
  },
  topo: {
    fontSize:26,
    marginTop:40,
    alignItems:'center'
  },
  botoesTopo:{
    borderRadius:5,
    backgroundColor:'#403DDA',
    padding:10,
    marginLeft:10
  },
  white:{
    color:"#fff"
  },
  gridContas:{
    flex:4,
    flexDirection:"row",
    flexWrap:"wrap",
    marginTop:20
  }
});
